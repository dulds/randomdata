import randomdata.generators as generators
import unittest

class ConstantGeneratorTest(unittest.TestCase):
    def setUp(self):
        self.generator = generators.Constant(value='CONSTANT')

    def test_Constant(self):
        self.assertEqual(self.generator.generate(), 'CONSTANT')


class IntGeneratorTest(unittest.TestCase):
    def test_intgenerator1(self):
        g1 = generators.Int()  #default
        value = g1.generate()
        import sys
        maximum = sys.maxint
        self.assertTrue( 0 <= int(value) <= maximum )

    def test_intgenerator2(self):
        g2 = generators.Int(minimum=10, maximum=10)
        value2 = g2.generate()
        self.assertEqual(int(value2), 10)

    def test_intgenerator3(self):
        g3 = generators.Int(minimum=1, maximum=1, format='%02d')
        value3 = g3.generate()
        self.assertEqual(value3, '01')

class IntIDGeneratorTest(unittest.TestCase):
    def test_intidgenerator1(self):
        g1 = generators.IntID() #default
        self.assertEqual(g1.generate(), 0)
        self.assertEqual(g1.generate(), 1)
        self.assertEqual(g1.generate(), 2)


    def test_intidgenerator2(self):
        g2 = generators.IntID(start=10)
        self.assertEqual(g2.generate(), 10)
        self.assertEqual(g2.generate(), 11)

    def test_intidgenerator3(self):
        g3 = generators.IntID(start=10, step=20)
        self.assertEqual(g3.generate(), 10)
        self.assertEqual(g3.generate(), 30)

class ItemFromListGeneratorTest(unittest.TestCase):
    def test_itemfromlist1(self):
        g1 = generators.ItemFromList(choices=['1'])
        self.assertEqual(g1.generate(), '1')

    def test_itemfromlist2(self):
        g2 = generators.ItemFromList(choices=['a', 'b'])
        self.assertTrue(g2.generate() in 'ab')


class StringFromRegexGeneratorTest(unittest.TestCase):
    def test_regextest1(self):
        g1 = generators.StringFromRegex(regex='^1$')
        self.assertEqual(g1.generate(), '1')

    def test_regextest2(self):
        g2 = generators.StringFromRegex(regex='^[A-Z]$')
        value = g2.generate()
        self.assertTrue( value.isalpha() and value.isupper())

class IncrementalDateTimeGeneratorTest(unittest.TestCase):
    def test_datetimewithformat(self):
        from datetime import datetime
        g3 = generators.IncrementalDateTime(
                 min_datetime= datetime(2013,12,13,12,34,56),
                 format='%Y%m%d%H%M%S',
                 ms_delta_range=0
             )
        value = g3.generate()
        self.assertEqual(value, '20131213123456')
       
if __name__ == '__main__':
    unittest.main()

